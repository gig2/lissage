How to build

Either with meson

* First we need to have OpenMesh with an existing pkg-config file
 For that we will use tho fork at
 https://github.com/gig2/OpenMesh
 using the branch: add-freebsd-support
 this branch add an openmesh.pc.in for future use
 
 * After building and installing OpenMesh in a given prefix : $PREFIX
 we just need to call from the source directory
 
 export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig # or $PREFIX/libdata/pkgconfig
 meson $BUILDDIR 
 cd $BUILDDIR 
 ninja
 
 
 Note that the dependency are : qt5, glew, glm, eigen3, opengl
 
Or with cmake 

here are the options that should be disabled
for increasing build speed(Openmesh apps)
 
* BUILD_APPS -> OFF
