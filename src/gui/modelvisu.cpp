#include "modelvisu.h"

#ifndef GLM_ENABLE_EXPERIMENTAL
#define GLM_ENABLE_EXPERIMENTAL
#endif
#include <glm/gtx/euler_angles.hpp>

#include "fill_hole.h"
#include "laplace.h"
#include "mesh_utils.h"
#include "valence.h"

#include <algorithm>


// bloomenthal have too much clash with other library
#include "bloomenthal.h"

#include <memory>

#include <OpenMesh/Core/IO/MeshIO.hh>

template <typename MeshType>
void resetColors( MeshType &mesh )
{
    auto value    = 0;
    auto maxValue = 100;

    // set color for mesh
    std::for_each( mesh.vertices_begin(), mesh.vertices_end(),
                   [&mesh, &value, maxValue]( auto &vertex ) {
                       Mesh::MeshT::Color color1{0.25, 0.5, 0.5};
                       Mesh::MeshT::Color color2{0.55, 0.7, 0.7};

                       float coeff = value / static_cast<float>( maxValue );

                       auto color = color1 * ( 1.f - coeff ) + color2 * coeff;

                       ++value;
                       value %= maxValue;

                       mesh.set_color( vertex, color );
                   } );
}


template <typename MeshType>
void applyGaussian( MeshType &mesh )
{
    //
    std::vector<float> gaussianValues;
    std::transform( mesh.vertices_begin(), mesh.vertices_end(),
                    std::back_inserter( gaussianValues ),
                    [&mesh]( auto const &vh ) //
                    {
                        auto angles = compute1RingAngles( mesh, vh );
                        auto area   = compute1RingArea( mesh, vh );

                        return ( 2. * M_PI - angles ) / area;
                    } );

    auto minmax = std::minmax_element( std::begin( gaussianValues ), std::end( gaussianValues ) );


    int index{0};

    std::for_each( mesh.vertices_begin(), mesh.vertices_end(),
                   [&mesh, &minmax, &gaussianValues, &index]( auto &vertex ) {
                       using Color = typename MeshType::Color;

                       Color color0{0.0, .0, 1.0};
                       Color color1{1.0, .0, 0.0};

                       Color color{0., 0., 0.};

                       auto gaussCurve = gaussianValues[ index ];

                       ++index;


                       float coeff = ( gaussCurve - *minmax.first )
                                     / static_cast<float>( *minmax.second - *minmax.first );

                       for ( int iCol = 0; iCol < 3; ++iCol )
                       {
                           color[ iCol ] = ( 1. - coeff ) * color0[ iCol ] + coeff * color1[ iCol ];
                       }

                       mesh.set_color( vertex, color );
                   } );
}

template <typename MeshType>
void applyMean( MeshType &mesh )
{
    //

    OpenMesh::VPropHandleT<float> meanCurveId;

    mesh.add_property( meanCurveId, "meanCurveId" );

    std::for_each( mesh.vertices_begin(), mesh.vertices_end(),
                   [&mesh, &meanCurveId]( auto const &vh ) //
                   { mesh.property( meanCurveId, vh ) = 0.f; } );

    std::for_each( mesh.edges_begin(), mesh.edges_end(),
                   [&mesh, &meanCurveId]( auto const &eh ) //
                   {
                       if ( !mesh.is_boundary( eh ) )
                       {
                           //
                           auto heh = mesh.halfedge_handle( eh, 0 );

                           auto nextFace = mesh.face_handle( heh );

                           auto previousFace = mesh.opposite_face_handle( heh );

                           float diedreAngles = angleBetweenFaces( mesh, previousFace, nextFace );

                           auto fromVertex = mesh.from_vertex_handle( heh );
                           auto toVertex   = mesh.to_vertex_handle( heh );

                           auto edge = mesh.point( fromVertex ) - mesh.point( toVertex );

                           float meanCurve{0.5f * diedreAngles
                                           * static_cast<float>( norm( edge ) )};

                           mesh.property( meanCurveId, fromVertex )
                               += meanCurve / static_cast<float>( mesh.valence( fromVertex ) );
                           mesh.property( meanCurveId, toVertex )
                               += meanCurve / static_cast<float>( mesh.valence( toVertex ) );
                       }
                   } );

    auto minmax = std::minmax_element(
        mesh.vertices_begin(), mesh.vertices_end(),
        [&mesh, &meanCurveId]( auto const &lhs,
                               auto const &rhs ) //
        { return mesh.property( meanCurveId, lhs ) < mesh.property( meanCurveId, rhs ); } );


    std::for_each(
        mesh.vertices_begin(), mesh.vertices_end(), [&mesh, &minmax, &meanCurveId]( auto &vertex ) {
            using Color = typename MeshType::Color;

            Color color0{0.0, .0, 1.0};
            Color color1{1.0, .0, 0.0};

            Color color{0., 0., 0.};

            auto meanCurve = mesh.property( meanCurveId, vertex );

            auto minValue = mesh.property( meanCurveId, *minmax.first );

            auto maxValue = mesh.property( meanCurveId, *minmax.second );



            float coeff = ( meanCurve - minValue ) / static_cast<float>( maxValue - minValue );

            for ( int iCol = 0; iCol < 3; ++iCol )
            {
                color[ iCol ] = ( 1. - coeff ) * color0[ iCol ] + coeff * color1[ iCol ];
            }

            mesh.set_color( vertex, color );
        } );
}


ModelVisu::ModelVisu( QWidget *parent )
    : QOpenGLWidget( parent )
{
    int major = 3;
    int minor = 2;

    QSurfaceFormat format;

    format.setDepthBufferSize( 24 );
    format.setStencilBufferSize( 8 );
    format.setVersion( major, minor );
    format.setProfile( QSurfaceFormat::CoreProfile );

    setFormat( format );

    create();

    objTransform_ = glm::rotate( objTransform_, glm::radians( 90.f ), glm::vec3( 1, 0, 0 ) );

    eulerTransform_ = glm::eulerAngleXYZ( yaw, pitch, roll );

    scaleTransform_ = glm::scale( glm::mat4{}, glm::vec3{scale_, scale_, scale_} );
}



void ModelVisu::initializeGL()
{
    glewExperimental = GL_TRUE;
    GLenum initGlew{glewInit()};

    if ( initGlew != GLEW_OK )
    {
        throw std::runtime_error(
            reinterpret_cast<const char *>( glewGetErrorString( initGlew ) ) );
    }



    simpleShader_.SetFile( "shader/color.vert", "shader/color.frag", "shader/color.geom" );


    modelview_
        = glm::lookAt( glm::vec3( -1., -1., 1. ), glm::vec3( 0., 0., 0. ), glm::vec3( 0, 0, 1 ) );

    //
    glClearColor( .0f, 0.f, 0.f, .0f );
}

void ModelVisu::resizeGL( int width, int height )
{
    //
    float near = 0.01;
    float far  = 100;
    float fov  = 70.;

    projection_ = glm::perspective( fov, static_cast<float>( width ) / height, near, far );
}

void ModelVisu::paintGL()
{
    //
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glEnable( GL_DEPTH_TEST );
    simpleShader_.Enable();

    glm::mat4 mvp = projection_ * modelview_;
    mvp           = glm::rotate( mvp, glm::radians( rotateAngle ), glm::vec3{0.f, 0.f, 1.f} );

    auto mvpLoc = simpleShader_.GetUniformLocation( "MVP" );

    // applyBunny Transforms

    glm::mat4 bunnyMvp = mvp * objTransform_ * eulerTransform_ * scaleTransform_;

    glUniformMatrix4fv( mvpLoc, 1, GL_FALSE, glm::value_ptr( bunnyMvp ) );

    for ( auto const &node : meshNode_ )
    {
        node->draw();
    }

    simpleShader_.Disable();
}


void ModelVisu::addMesh( QString model )
{
    auto meshPtr = std::make_shared<Mesh>( model.toStdString() );
    mesh_.push_back( meshPtr );

    auto &meshView = *meshPtr;

    auto meshNodePtr = std::make_shared<MeshNode<Mesh>>( meshView );

    meshNode_.push_back( meshNodePtr );

    auto &mesh = meshPtr->mesh;

    ::resetColors( mesh );

    auto &meshNode = *meshNodePtr;



    meshView.refreshBuffer();

    makeCurrent();
    meshNode.updateVertexBuffer();
    doneCurrent();

    // we need to repaint the widget
    update();
}

double sphere( double x, double y, double z )
{
    return -( x * x + y * y + z * z - 1.0 );
}

void ModelVisu::rotateAngleChanged( int newAngle )
{
    rotateAngle = newAngle;

    update();
}

void ModelVisu::addBloomenthal()
{
    objTransform_ = glm::scale( glm::mat4{}, glm::vec3{0.6, 0.6, 0.6} );

    //
    auto meshPtr = std::make_shared<Mesh>();

    mesh_.push_back( meshPtr );

    bloomenthal bloom{};


    // polygonalize

    // char const *polyResult
    //     = bloom.polygonize( sphere, 0.1, 15, 0., 0., 0.1, bloom.triangle2, NOTET );
    // take so much time, but give good result
    char const *polyResult
        = bloom.polygonize( sphere, 0.01, 300, 0., 0., 0.1, bloom.triangle2, NOTET );

    if ( polyResult != nullptr )
    {
        std::cout << "Error: " << polyResult << "\n";
    }

    // end polygonalize
    auto &meshView = *meshPtr;

    auto meshNodePtr = std::make_shared<MeshNode<Mesh>>( meshView );

    meshNode_.push_back( meshNodePtr );


    auto &meshNode = *meshNodePtr;

    bloom.createMeshOpenMesh( meshView.mesh );


    ::resetColors( meshView.mesh );


    meshView.refreshBuffer();

    makeCurrent();
    meshNode.updateVertexBuffer();
    doneCurrent();

    update();
}

void ModelVisu::applyValence()
{
    makeCurrent();
    valenceColorMap();
    doneCurrent();

    update();
}

void ModelVisu::applyLaplace()
{
    // std::vector<std::pair<float, float>> minmax;

    makeCurrent();
    for ( auto &mesh : mesh_ )
    {
        ::applyLaplace( mesh->mesh );
        // minmax.push_back(::applyLaplace( mesh->mesh ) );
        mesh->refreshBuffer();
    }

    for ( auto &meshNode : meshNode_ )
    {
        meshNode->updateVertexBuffer();
    }

    doneCurrent();


    // currentMinLaplace( static_cast<double>( minmax.front().first ) );
    // currentMaxLaplace( static_cast<double>( minmax.front().second ) );

    update();
}

void ModelVisu::applyLaplaceCotan()
{
    makeCurrent();
    for ( auto &mesh : mesh_ )
    {
        ::applyLaplaceCotan( mesh->mesh );
        mesh->refreshBuffer();
    }

    for ( auto &meshNode : meshNode_ )
    {
        meshNode->updateVertexBuffer();
    }

    doneCurrent();

    update();
}



void ModelVisu::resetColors()
{
    //
    for ( auto &mesh : mesh_ )
    {
        ::resetColors( mesh->mesh );
        mesh->refreshBuffer();
    }

    makeCurrent();
    for ( auto &meshNode : meshNode_ )
    {
        meshNode->updateVertexBuffer();
    }
    doneCurrent();

    update();
}

void ModelVisu::valenceColorMap()
{
    for ( auto &meshPtr : mesh_ )
    {
        //
        ::applyValence( meshPtr->mesh );
        meshPtr->refreshBuffer();
    }

    for ( auto &meshNode : meshNode_ )
    {
        //
        meshNode->updateVertexBuffer();
    }
}

void ModelVisu::fillHole()
{
    for ( auto &meshPtr : mesh_ )
    {
        // we should definitly save it somewhere
        auto fillHole = FillHoleInMesh{meshPtr->mesh};

        auto const &tag = fillHole.getTag();

        auto number = tag.numberOfHoles();

        std::cout << "Number of holes: " << number << " \n";

        meshPtr->refreshBuffer();
    }
    makeCurrent();
    for ( auto &meshNode : meshNode_ )
    {
        meshNode->updateVertexBuffer();
    }
    doneCurrent();
    update();
}

void ModelVisu::setYaw( int value )
{
    yaw = glm::radians( static_cast<float>( value ) );
    updateEuler();
    update();
}

void ModelVisu::setPitch( int value )
{
    pitch = glm::radians( static_cast<float>( value ) );
    updateEuler();
    update();
}

void ModelVisu::setRoll( int value )
{
    roll = glm::radians( static_cast<float>( value ) );
    updateEuler();
    update();
}

void ModelVisu::setScale( double scale )
{
    scale_          = scale;
    scaleTransform_ = glm::scale( glm::mat4{}, glm::vec3{scale_, scale_, scale_} );
    update();
}

void ModelVisu::updateEuler()
{
    eulerTransform_ = glm::eulerAngleXYZ( yaw, pitch, roll );
}

void ModelVisu::applyGaussian()
{
    //
    makeCurrent();
    for ( auto &mesh : mesh_ )
    {
        ::applyGaussian( mesh->mesh );
        mesh->refreshBuffer();
    }

    for ( auto &meshNode : meshNode_ )
    {
        meshNode->updateVertexBuffer();
    }

    doneCurrent();

    update();
}

void ModelVisu::applyMean()
{
    //
    makeCurrent();
    for ( auto &mesh : mesh_ )
    {
        ::applyMean( mesh->mesh );
        mesh->refreshBuffer();
    }

    for ( auto &meshNode : meshNode_ )
    {
        meshNode->updateVertexBuffer();
    }

    doneCurrent();

    update();
}
