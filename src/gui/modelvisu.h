#pragma once

#include <GL/glew.h>

#include "OpenGLMeshRender/meshnode.h"
#include "OpenGLShader/shader.h"
#include "mesh.h"


#include <QOpenGLWidget>

#ifndef GLM_FORCE_RADIANS
#define GLM_FORCE_RADIANS
#endif
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <memory>
#include <vector>

class ModelVisu : public QOpenGLWidget
{
    Q_OBJECT
public:
    explicit ModelVisu( QWidget *parent );


signals:
    void currentMinLaplace( double value );
    void currentMaxLaplace( double value );

protected:
    void initializeGL() override;

    void resizeGL( int width, int height ) override;

    void paintGL() override;


public slots:
    void addMesh( QString model );
    void addBloomenthal();
    void applyValence();
    void applyLaplace();
    void resetColors();

    void rotateAngleChanged( int );

    void setYaw( int );
    void setPitch( int );
    void setRoll( int );
    void setScale( double );

    void applyLaplaceCotan();

    void fillHole();

    void applyGaussian();
    void applyMean();


private:
    // get them from shader
    int const positionLocation_{0};
    int const colorLocation_{1};

    std::vector<std::shared_ptr<Mesh>> mesh_;
    std::vector<std::shared_ptr<MeshNode<Mesh>>> meshNode_;

    S3DE::Shader simpleShader_;

    glm::mat4 objTransform_{1.f};

    glm::mat4 projection_;
    glm::mat4 modelview_;

    void valenceColorMap();

    float rotateAngle{0.f};

    glm::mat4 eulerTransform_{1.f};

    glm::mat4 scaleTransform_{1.f};

    float yaw{0.};
    float pitch{0.};
    float roll{0.};

    float scale_{1.};

    void updateEuler();
};
