#pragma once
#include <OpenMesh/Core/Mesh/Traits.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>


#include "mesh_utils.h"

#include <map>
#include <optional>


template <typename MeshType, typename HalfedgeInputIt, typename HalfedgeOutputIt>
HalfedgeOutputIt partitionBoundary( MeshType& mesh, OpenMesh::VPropHandleT<int> const& idHole,
                                    int const number, HalfedgeInputIt halfedgeFirstIn,
                                    HalfedgeInputIt halfedgeLastIn, HalfedgeOutputIt halfedgeOut );




template <typename MeshType, typename HalfedgeInputIt, typename HalfedgeOutputIt>
HalfedgeOutputIt computeOrderedHandle( MeshType const& mesh,
                                       OpenMesh::VPropHandleT<int> const& idHole, int const number,
                                       HalfedgeInputIt firstIn, HalfedgeInputIt lastIn,
                                       HalfedgeOutputIt lastOut );




template <typename MeshType>
class TagHole
{
public:
    using HalfedgeHandle        = typename MeshType::HalfedgeHandle;
    using HalfedgeContainer     = std::vector<HalfedgeHandle>;
    using HalfedgeHandleItType  = decltype( std::declval<HalfedgeContainer>().begin() );
    using HalfedgeHandleCItType = decltype( std::declval<HalfedgeContainer>().cbegin() );

    using Range  = std::pair<HalfedgeHandleItType, HalfedgeHandleItType>;
    using CRange = std::pair<HalfedgeHandleCItType, HalfedgeHandleCItType>;

    explicit TagHole( MeshType& mesh );

    int numberOfHoles() const { return numberOfHoles_; }

    std::optional<CRange> getHandle( int const number ) const;

private:
    MeshType& mesh_;
    int numberOfHoles_{0};
    OpenMesh::VPropHandleT<int> idHole_;

    std::map<int, HalfedgeContainer> handles_;

    /*** \brief tag hole with hole number, return halfedgeHandles vector, and set the
     * totalenumberOfHoles
     *
     */
    std::vector<HalfedgeHandle> tagHole_();
};


#include "private/tag_hole_internal.h"
