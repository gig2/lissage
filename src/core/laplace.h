#pragma once

#include "mesh_utils.h"
#include "triangle_area.h"

#include <algorithm>
#include <cmath>
#include <functional>
#include <vector>

#include <OpenMesh/Core/Geometry/Vector11T.hh>
#include <OpenMesh/Core/Mesh/Traits.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

#include <type_traits>


#include <Eigen/Dense>


template <typename Vector>
auto cotangente( Vector e1, Vector e2 )
{
    e1.normalize();
    e2.normalize();

    auto cos_e1_e2 = dot( e1, e2 );
    // restrict cos to -1.f , 1.f

    auto e3 = cross( e1, e2 );

    auto abs_sin_e1_e2 = norm( e3 );

    // e1 and e2 have to be non colinear

    decltype( abs_sin_e1_e2 ) minValue{1.e-6};


    if ( abs_sin_e1_e2 > minValue )
    {
        return cos_e1_e2 / abs_sin_e1_e2;
    }
    else // else we simply 1/sin
    {
        return decltype( minValue ){1.} / minValue;
    }
}



#include <iostream>

template <typename MeshType>
class LaplaceBeltramy
{
public:
    LaplaceBeltramy( MeshType &mesh )
        : meshView_{mesh}
        , laplace_{meshView_.n_vertices(), meshView_.n_vertices()}
        , positionsX_{meshView_.n_vertices(), 1}
        , positionsY_{meshView_.n_vertices(), 1}
        , positionsZ_{meshView_.n_vertices(), 1}
    {
    }

    template <typename Vertex>
    auto computeArea( Vertex const &vertex )
    {
        return compute1RingArea( meshView_, vertex );
    }

    template <typename FacingVertex>
    auto omega( FacingVertex const &prevFacing, FacingVertex const &facingVertex,
                FacingVertex const &nextFacing )
    {
        auto e1 = prevFacing - facingVertex;

        auto e2 = -prevFacing;

        auto cotA = cotangente( e1, e2 );


        e1 = -nextFacing;
        e2 = facingVertex - nextFacing;

        auto cotB = cotangente( e1, e2 );

        return cotA + cotB;
    }

    template <typename Vertex>
    auto sumOmega( Vertex const &vertex )
    {
        float accumulate{0.f};

        std::vector<typename MeshType::Point> vertexToVoisin;

        for ( auto vvIt = meshView_.vv_iter( vertex ); vvIt.is_valid(); ++vvIt )
        {
            //
            auto const &voisin = meshView_.point( *vvIt );

            auto const &point = meshView_.point( vertex );

            vertexToVoisin.push_back( voisin - point );
        }

        for ( auto vertexIt = std::begin( vertexToVoisin ); vertexIt != std::end( vertexToVoisin );
              ++vertexIt )
        {
            auto prevIt   = vertexIt;
            auto facingIt = std::next( prevIt );
            if ( facingIt == std::end( vertexToVoisin ) )
            {
                facingIt = std::begin( vertexToVoisin );
            }
            auto nextIt = std::next( facingIt );
            if ( nextIt == std::end( vertexToVoisin ) )
            {
                nextIt = std::begin( vertexToVoisin );
            }
            accumulate += omega( *prevIt, *facingIt, *nextIt );
        }


        return accumulate;
    }


    void computeLaplace()
    {
        laplace_.fill( 0.f );

        for ( auto vIt = meshView_.vertices_begin(); vIt != meshView_.vertices_end(); ++vIt )
        {
            //
            auto i = std::distance( meshView_.vertices_begin(), vIt );

            /* auto area = 2.f * computeArea( *vIt ); */

            auto area = static_cast<float>( meshView_.valence( *vIt ) );

            laplace_( i, i ) = -sumOmega( *vIt ) / area;

            std::vector<typename MeshType::Point> vertexToVoisin;

            for ( auto vvIt = meshView_.vv_iter( *vIt ); vvIt.is_valid(); ++vvIt )
            {
                //
                auto const &voisin = meshView_.point( *vvIt );

                auto const &point = meshView_.point( *vIt );

                vertexToVoisin.push_back( voisin - point );
            }

            for ( auto vertexIt = std::begin( vertexToVoisin );
                  vertexIt != std::end( vertexToVoisin ); ++vertexIt )
            {
                auto prevIt   = vertexIt;
                auto facingIt = std::next( prevIt );
                if ( facingIt == std::end( vertexToVoisin ) )
                {
                    facingIt = std::begin( vertexToVoisin );
                }
                auto nextIt = std::next( facingIt );
                if ( nextIt == std::end( vertexToVoisin ) )
                {
                    nextIt = std::begin( vertexToVoisin );
                }


                auto w = omega( *prevIt, *facingIt, *nextIt );


                auto voisinNumber = std::distance( std::begin( vertexToVoisin ), facingIt );

                auto vvIt = meshView_.vv_iter( *vIt );
                std::advance( vvIt, voisinNumber );

                auto targetVertexIt
                    = std::find( meshView_.vertices_begin(), meshView_.vertices_end(), *vvIt );

                if ( targetVertexIt != meshView_.vertices_end() )
                {
                    auto j = std::distance( meshView_.vertices_begin(), targetVertexIt );

                    laplace_( i, j ) = w / area;
                }
                else
                {
                    throw std::runtime_error( "cannot find vertex handle" );
                }
            }



            positionsX_( i, 0 ) = meshView_.point( *vIt )[ dirX ];
            positionsY_( i, 0 ) = meshView_.point( *vIt )[ dirY ];
            positionsZ_( i, 0 ) = meshView_.point( *vIt )[ dirZ ];
        }

        // finnaly we can compute L

        positionsX_ = laplace_ * positionsX_;
        positionsY_ = laplace_ * positionsY_;
        positionsZ_ = laplace_ * positionsZ_;

        for ( auto vIt = meshView_.vertices_begin(); vIt != meshView_.vertices_end(); ++vIt )
        {
            auto i = std::distance( meshView_.vertices_begin(), vIt );

            auto const &xi = meshView_.point( *vIt );

            auto u = xi;
            auto f = decltype( u ){positionsX_( i, 0 ), positionsY_( i, 0 ), positionsZ_( i, 0 )};

            float h      = 2.e-2f;
            float lambda = 1.f;

            u += h * lambda * f;

            meshView_.set_point( *vIt, u );
        }
    }


private:
    MeshType &meshView_;

    static constexpr unsigned int dirX = 0;
    static constexpr unsigned int dirY = 1;
    static constexpr unsigned int dirZ = 2;

    Eigen::MatrixXf laplace_;

    Eigen::MatrixXf positionsX_;
    Eigen::MatrixXf positionsY_;
    Eigen::MatrixXf positionsZ_;
};


template <typename MeshType>
void applyLaplace( MeshType &mesh )
{
    int const numIter = 10;
    for ( int iter = 0; iter < numIter; ++iter )
    {
        std::for_each( mesh.vertices_begin(), mesh.vertices_end(), [&mesh]( auto &vertex ) {
            auto h      = 2.e-1f;
            auto lambda = 1.f;

            auto const &xi = mesh.point( vertex );

            auto u = xi;


            auto f = 0.f * xi;

            for ( auto vvIt = mesh.vv_iter( vertex ); vvIt.is_valid(); ++vvIt )
            {
                f += mesh.point( *vvIt ) - xi;
            }

            u += h * lambda * ( 1.f / static_cast<float>( mesh.valence( vertex ) ) ) * f;


            mesh.set_point( vertex, u );
        } );
    }
}


template <typename MeshType>
void applyLaplaceCotanDirect( MeshType &mesh )
{
    int const numIter = 1;
    for ( int iter = 0; iter < numIter; ++iter )
    {
        LaplaceBeltramy<MeshType> laplace{mesh};

        std::for_each(
            mesh.vertices_begin(), mesh.vertices_end(), [&mesh, &laplace]( auto &vertex ) {
                auto h      = 2.e-1f;
                auto lambda = 1.f;

                auto area = 2.f * laplace.computeArea( vertex );

                auto epsilon = 1.e-1f;
                if ( area < epsilon )
                {
                    area = epsilon;
                }

                auto const &xi = mesh.point( vertex );

                auto u = xi;

                /* auto facing_init   = mesh.halfedge_handle( vertex ); */
                /* auto currentFacing = facing_init; */


                auto f = 0.f * xi;

                std::vector<typename MeshType::Point> vertexToVoisin;

                for ( auto vvIt = mesh.vv_iter( vertex ); vvIt.is_valid(); ++vvIt )
                {
                    //
                    auto const &voisin = mesh.point( *vvIt );

                    auto const &point = mesh.point( vertex );

                    vertexToVoisin.push_back( voisin - point );
                }

                for ( auto vertexIt = std::begin( vertexToVoisin );
                      vertexIt != std::end( vertexToVoisin ); ++vertexIt )
                {
                    auto prevIt   = vertexIt;
                    auto facingIt = std::next( prevIt );
                    if ( facingIt == std::end( vertexToVoisin ) )
                    {
                        facingIt = std::begin( vertexToVoisin );
                    }
                    auto nextIt = std::next( facingIt );
                    if ( nextIt == std::end( vertexToVoisin ) )
                    {
                        nextIt = std::begin( vertexToVoisin );
                    }


                    auto w = laplace.omega( *prevIt, *facingIt, *nextIt );

                    auto voisinNumber = std::distance( std::begin( vertexToVoisin ), facingIt );

                    auto vvIt = mesh.vv_iter( vertex );
                    std::advance( vvIt, voisinNumber );

                    auto targetVertexIt
                        = std::find( mesh.vertices_begin(), mesh.vertices_end(), *vvIt );

                    if ( targetVertexIt != mesh.vertices_end() )
                    {
                        auto j = std::distance( mesh.vertices_begin(), targetVertexIt );

                        f += w * mesh.point( *targetVertexIt ) - xi;
                    }
                }


                u += h * lambda * ( 1.f / area ) * f;


                mesh.set_point( vertex, u );
            } );
    }
}


template <typename MeshType>
void applyLaplaceCotan( MeshType &mesh )
{
    int const maxIter = 10;
    LaplaceBeltramy<MeshType> laplace{mesh};
    for ( int iter = 0; iter < maxIter; ++iter )
    {
        laplace.computeLaplace();
    }
}
