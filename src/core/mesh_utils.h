#pragma once

#include "triangle_area.h"

#include <OpenMesh/Core/Geometry/Vector11T.hh>
#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <vector>


template <typename MeshType, typename HalfedgeHandle>
bool isInSameBound( MeshType const &mesh, HalfedgeHandle lhs, HalfedgeHandle rhs )
{
    //
    auto currHalfEdgeHandle = lhs;
    do
    {
        if ( !mesh.is_valid_handle( lhs ) || !mesh.is_valid_handle( rhs ) )
        {
            throw std::runtime_error( "either lhs or rhs halfedgeHandle are not valid" );
        }

        if ( currHalfEdgeHandle == rhs )
            return true;

        currHalfEdgeHandle = mesh.next_halfedge_handle( currHalfEdgeHandle );

    } while ( currHalfEdgeHandle != lhs );
    return false;
}


template <typename MeshType, typename VertexHandle>
float compute1RingArea( MeshType const &mesh, VertexHandle const &vertex )
{
    std::vector<typename MeshType::Point> vertexToVoisin;

    for ( auto vvIt = mesh.cvv_iter( vertex ); vvIt.is_valid(); ++vvIt )
    {
        //
        auto const &voisin = mesh.point( *vvIt );

        auto const &point = mesh.point( vertex );

        vertexToVoisin.push_back( voisin - point );
    }


    // this is std accumulate
    float accumulate = 0.f;

    for ( auto vertexIt = std::begin( vertexToVoisin ); vertexIt != std::end( vertexToVoisin );
          ++vertexIt )
    {
        //
        auto nextVoisin = std::next( vertexIt );
        if ( nextVoisin == std::end( vertexToVoisin ) )
        {
            break;
        }
        //
        // get the third vector
        auto third = *nextVoisin - *vertexIt;

        auto firstLength  = norm( *vertexIt );
        auto secondLength = norm( *nextVoisin );
        auto thirdLength  = norm( third );


        auto area = triangleArea( firstLength, secondLength, thirdLength );

        accumulate += static_cast<float>( area );
    }

    //

    return accumulate;
}


template <typename MeshType, typename VertexHandle>
float compute1RingAngles( MeshType const &mesh, VertexHandle const &vertex )
{
    std::vector<typename MeshType::Point> vertexToVoisin;

    for ( auto vvIt = mesh.cvv_iter( vertex ); vvIt.is_valid(); ++vvIt )
    {
        //
        auto const &voisin = mesh.point( *vvIt );

        auto const &point = mesh.point( vertex );

        vertexToVoisin.push_back( voisin - point );
    }


    // this is std accumulate
    float accumulate = 0.f;

    for ( auto vertexIt = std::begin( vertexToVoisin ); vertexIt != std::end( vertexToVoisin );
          ++vertexIt )
    {
        //
        auto nextVoisin = std::next( vertexIt );
        if ( nextVoisin == std::end( vertexToVoisin ) )
        {
            break;
        }

        auto e1 = *vertexIt;
        auto e2 = *nextVoisin;

        e1.normalize();
        e2.normalize();

        auto cosE1E2 = dot( e1, e2 );

        accumulate += std::acos( cosE1E2 );
    }

    return accumulate;
}

/** \brief Given a mesh and a faceHandle compute one normal from the first 3 vertices
 *
 * \pre faceHandle should be a valid handle for the mesh
 * \pre the face should have at least 3 vertices
 */
template <typename MeshType, typename FaceHandle>
auto normalFromTriangle( MeshType const &mesh, FaceHandle const &faceHandle )
{
    // Here we assume that a faces contains at least 3 vertices
    auto firstPointIt  = mesh.cfv_iter( faceHandle );
    auto secondPointIt = std::next( firstPointIt );
    auto thirdPointIt  = std::next( secondPointIt );

    auto vecFromPoint = [&mesh]( auto const &originIt, auto const &destinationIt ) {
        return mesh.point( *destinationIt ) - mesh.point( *originIt );
    };

    auto firstBaseElem = vecFromPoint( firstPointIt, secondPointIt );

    auto secondBaseElem = vecFromPoint( secondPointIt, thirdPointIt );

    return cross( firstBaseElem, secondBaseElem );
}



/** \brief Given a mesh and two faceHandle, compute the non signed angle between those two faces
 *
 * \pre lhs and rhs should each be a valid handle for the mesh
 * \pre each faces should have at least 3 vertices
 *
 */
template <typename MeshType, typename FaceHandle>
auto angleBetweenFaces( MeshType const &mesh, FaceHandle const &lhs, FaceHandle const &rhs )
{
    auto normalLhs = normalFromTriangle( mesh, lhs );

    auto normalRhs = normalFromTriangle( mesh, rhs );

    normalLhs.normalize();
    normalRhs.normalize();


    auto cosTheta = dot( normalLhs, normalRhs );

    // we need to restrict the cosTheta to -1.f , 1.f
    if ( cosTheta > 1.f )
    {
        cosTheta = 1.f;
    }
    else if ( cosTheta < -1.f )
    {
        cosTheta = -1.f;
    }

    return std::acos( cosTheta );
}
