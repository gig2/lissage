#pragma once

#include "tag_hole.h"

#include <OpenMesh/Core/Geometry/Vector11T.hh>
#include <OpenMesh/Core/Mesh/Traits.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

#include <algorithm>
#include <functional>

#include <iostream>
#include <vector>




template <typename MeshType>
class FillHoleInMesh
{
public:
    explicit FillHoleInMesh( MeshType& mesh );

    auto const& getTag() const { return tag_; }

private:
    MeshType& mesh_;
    TagHole<MeshType> tag_;

    void fill_();

    void recursiveFill_( typename TagHole<MeshType>::CRange const& nthHole );
};



/* template <typename MeshType> */
/* void fillHole( MeshType& mesh ); */




#include "private/fill_hole_internal.h"
