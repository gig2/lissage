set (core_sources
  mesh.cpp
  bloomenthal.cpp)
set (core_headers
  bloomenthal.h
  fill_hole.h
  mesh.h
  mesh_utils.h
  laplace.h
  tag_hole.h
  triangle_area.h
  valence.h)

set ( core_private_headers
  private/fill_hole_internal.h
  private/tag_hole_internal.h)


add_library(lissage_core ${core_sources} ${core_headers})

target_include_directories(lissage_core PUBLIC
  $<BUILD_INTERFACE:${OPENMESH_INCLUDE_DIRS}>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<INSTALL_INTERFACE:include/Lissage/core>)

target_link_libraries(lissage_core PUBLIC
  ${OPENMESH_LIBRARIES}
  Eigen3::Eigen)
