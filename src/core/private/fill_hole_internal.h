#pragma once
#include "../fill_hole.h"
#include "../triangle_area.h"

#include <OpenMesh/Core/Geometry/Vector11T.hh>
#include <OpenMesh/Core/Mesh/Traits.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

#include <algorithm>
#include <functional>

#include <iostream>
#include <vector>


template <typename MeshType>
FillHoleInMesh<MeshType>::FillHoleInMesh( MeshType& mesh )
    : mesh_{mesh}
    , tag_{mesh_}
{
    fill_();
}


template <typename MeshType>
void FillHoleInMesh<MeshType>::recursiveFill_( typename TagHole<MeshType>::CRange const& nthHole )
{
    auto lastHandleIt   = std::prev( nthHole.second );
    auto secondHandleIt = std::next( nthHole.first );


    auto firstPointHandle = mesh_.from_vertex_handle( *nthHole.first );
    auto thirdPointHandle = mesh_.from_vertex_handle( *lastHandleIt );

    if ( secondHandleIt == lastHandleIt )
    {
        throw std::runtime_error( "Not enought elements" );
    }

    if ( std::next( secondHandleIt ) == lastHandleIt )
    {
        std::vector<typename MeshType::VertexHandle> faceVHandle;

        auto secondPointHandle = mesh_.from_vertex_handle( *secondHandleIt );

        faceVHandle.push_back( firstPointHandle );
        faceVHandle.push_back( secondPointHandle );
        faceVHandle.push_back( thirdPointHandle );

        mesh_.add_face( faceVHandle );
        return;
    }

    std::vector<double> areas;

    auto firstPoint = mesh_.point( firstPointHandle );
    auto thirdPoint = mesh_.point( thirdPointHandle );

    auto v3 = firstPoint - thirdPoint;


    areas.reserve( std::distance( secondHandleIt, lastHandleIt ) );

    std::transform( secondHandleIt, lastHandleIt, std::back_inserter( areas ),
                    [this, &firstPoint, &thirdPoint, &v3]( auto const& heh ) //

                    {
                        if ( mesh_.is_valid_handle( heh ) )
                        {
                            auto secondPoint = mesh_.point( mesh_.from_vertex_handle( heh ) );

                            auto v1 = secondPoint - firstPoint;
                            auto v2 = thirdPoint - secondPoint;

                            double length1 = norm( v1 );
                            double length2 = norm( v2 );
                            double length3 = norm( v3 );


                            return triangleArea( length1, length2, length3 );
                        }
                        else
                        {
                            throw std::runtime_error( "Wrong handle" );
                        }
                    } );


    auto maxIt = std::max_element( std::begin( areas ), std::end( areas ) );

    auto shift = std::distance( std::begin( areas ), maxIt );


    std::vector<typename MeshType::VertexHandle> faceVHandle;

    if ( shift < std::distance( std::begin( areas ), std::end( areas ) ) )
    {
        auto candidateIndexIt = std::next( secondHandleIt, shift );

        auto secondPointHandle = mesh_.from_vertex_handle( *candidateIndexIt );

        faceVHandle.push_back( firstPointHandle );
        faceVHandle.push_back( secondPointHandle );
        faceVHandle.push_back( thirdPointHandle );

        mesh_.add_face( faceVHandle );

        using CRange = typename TagHole<MeshType>::CRange;

        auto left = CRange{nthHole.first, std::next( candidateIndexIt )};

        auto right = CRange{candidateIndexIt, nthHole.second};

        if ( std::distance( left.first, left.second ) >= 3 )
        {
            recursiveFill_( left );
        }
        if ( std::distance( right.first, right.second ) >= 3 )
        {
            recursiveFill_( right );
        }
    }
}

template <typename MeshType>
void FillHoleInMesh<MeshType>::fill_()
{
    //


    int const number = tag_.numberOfHoles();


    for ( int i = 0; i < number; ++i )
    {
        auto nthHole = tag_.getHandle( i );


        if ( nthHole )
        {
            recursiveFill_( *nthHole );
        }
    }
}
