#pragma once

#include "../tag_hole.h"

#include <optional>
#include <vector>

template <typename MeshType, typename HalfedgeInputIt, typename HalfedgeOutputIt>
HalfedgeOutputIt computeOrderedHandle( MeshType const& mesh,
                                       OpenMesh::VPropHandleT<int> const& idHole, int const number,
                                       HalfedgeInputIt firstIn, HalfedgeInputIt lastIn,
                                       HalfedgeOutputIt lastOut )
{
    auto firstHoleIt = std::find_if( firstIn, lastIn,
                                     [&mesh, &idHole, &number]( auto const& halfedgeHandle ) //
                                     {
                                         auto fromVertex
                                             = mesh.from_vertex_handle( halfedgeHandle );

                                         return mesh.property( idHole, fromVertex ) == number;
                                     } );


    if ( firstHoleIt != lastIn )
    {
        //
        auto firstHalfedgeHandle = *firstHoleIt;
        auto currHalfedgeHandle  = firstHalfedgeHandle;
        do
        {
            if ( !mesh.is_valid_handle( currHalfedgeHandle ) )
            {
                throw std::runtime_error( "current halfedge Handle is invalid" );
            }
            *lastOut = currHalfedgeHandle;
            ++lastOut;
            currHalfedgeHandle = mesh.next_halfedge_handle( currHalfedgeHandle );

        } while ( currHalfedgeHandle != firstHalfedgeHandle );
    }
    return lastOut;
}




template <typename MeshType, typename HalfedgeInputIt, typename HalfedgeOutputIt>
HalfedgeOutputIt partitionBoundary( MeshType& mesh, OpenMesh::VPropHandleT<int> const& idHole,
                                    int const number, HalfedgeInputIt halfedgeFirstIn,
                                    HalfedgeInputIt halfedgeLastIn, HalfedgeOutputIt halfedgeOut )
{
    //
    using HalfedgeHandle = typename MeshType::HalfedgeHandle;
    std::vector<HalfedgeHandle> trueVector;

    if ( std::distance( halfedgeFirstIn, halfedgeLastIn ) == 0 )
        return halfedgeOut;

    auto firstHandle = *halfedgeFirstIn;

    std::partition_copy( halfedgeFirstIn, halfedgeLastIn, std::back_inserter( trueVector ),
                         halfedgeOut,
                         [&mesh, &firstHandle]( auto const& halfEdgeHandle ) //
                         { return isInSameBound( mesh, firstHandle, halfEdgeHandle ); } );


    std::for_each( std::begin( trueVector ), std::end( trueVector ),
                   [&mesh, &number, &idHole]( auto const& halfedgeHandle ) //
                   {
                       auto fromVertex = mesh.from_vertex_handle( halfedgeHandle );
                       auto toVertex   = mesh.to_vertex_handle( halfedgeHandle );

                       mesh.property( idHole, fromVertex ) = number;
                       mesh.property( idHole, toVertex )   = number;
                   }

    );

    return halfedgeOut;
}




template <typename MeshType>
TagHole<MeshType>::TagHole( MeshType& mesh )
    : mesh_{mesh}
{
    //
    mesh_.add_property( idHole_, "idHole" );

    auto halfedgeHandles = tagHole_();

    for ( int i = 0; i < numberOfHoles_; ++i )
    {
        std::vector<HalfedgeHandle> nthHole;
        computeOrderedHandle( mesh_, idHole_, i, std::begin( halfedgeHandles ),
                              std::end( halfedgeHandles ), std::back_inserter( nthHole ) );

        handles_[ i ] = nthHole;
    }

    for ( auto const& handle : handles_ )
    {
        auto const& hole = handle.second;
        for ( auto const& heh : hole )
        {
            if ( !mesh_.is_valid_handle( heh ) )
            {
                throw std::runtime_error( "There is some invalid halfedgeHandle" );
            }
        }
    }
}

template <typename MeshType>
std::optional<typename TagHole<MeshType>::CRange>
TagHole<MeshType>::getHandle( int const number ) const
{
    auto handleIt = handles_.find( number );

    if ( handleIt != std::end( handles_ ) )
    {
        auto const& halfedgeVector = handleIt->second;
        return std::make_pair( std::cbegin( halfedgeVector ), std::cend( halfedgeVector ) );
    }
    else
    {
        return {};
    }
}

template <typename MeshType>
std::vector<typename MeshType::HalfedgeHandle> TagHole<MeshType>::tagHole_()
{
    using HalfedgeHandle = typename MeshType::HalfedgeHandle;

    std::vector<HalfedgeHandle> halfedgeHandles;

    std::copy_if( mesh_.halfedges_begin(), mesh_.halfedges_end(),
                  std::back_inserter( halfedgeHandles ),
                  [this]( auto const& halfEdgeHandle ) //
                  { return mesh_.is_boundary( halfEdgeHandle ); } );



    std::for_each( std::begin( halfedgeHandles ), std::end( halfedgeHandles ),
                   [this]( auto const& edgeHandle ) //
                   {
                       auto fromVertex = mesh_.from_vertex_handle( edgeHandle );
                       auto toVertex   = mesh_.to_vertex_handle( edgeHandle );

                       mesh_.property( idHole_, fromVertex ) = -1;
                       mesh_.property( idHole_, toVertex )   = -1;
                   }

    );


    int number = 0;

    std::vector<HalfedgeHandle> trueVector;


    partitionBoundary( mesh_, idHole_, number, std::begin( halfedgeHandles ),
                       std::end( halfedgeHandles ), std::back_inserter( trueVector ) );


    while ( !trueVector.empty() )
    {
        ++number;
        std::vector<HalfedgeHandle> workingVector;
        partitionBoundary( mesh_, idHole_, number, std::begin( trueVector ), std::end( trueVector ),
                           std::back_inserter( workingVector ) );

        std::swap( trueVector, workingVector );
    }

    // in case we have 0 hole, we will have number equal to 0
    // but halfedgeHandles size will be null
    // totalNumberOfHoles is number + 1 except if there is no hole
    if ( halfedgeHandles.size() > 0 )
        numberOfHoles_ = number + 1;

    return halfedgeHandles;
}
