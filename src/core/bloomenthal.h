#ifndef BLOOMENTHAL_H
#define BLOOMENTHAL_H

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <array>
#include <iostream>
#include <list>
#include <memory>
#include <vector>

#include "mesh.h"

/* ADAPTED FROM
 * C code from the article
 * "An Implicit Surface Polygonizer"
 * http::www.unchainedgeometry.com/jbloom/papers/polygonizer.pdf
 * by Jules Bloomenthal, jules@bloomenthal.com
 * in "Graphics Gems IV", Academic Press, 1994 */

/* implicit.c
 *     an implicit surface polygonizer, translated from Mesa
 *     applications should call polygonize()
 *
 * To compile a test program for ASCII output:
 *     cc implicit.c -o implicit -lm
 *
 * To compile a test program for display on an SGI workstation:
 *     cc -DSGIGFX implicit.c -o implicit -lgl_s -lm
 *
 * Authored by Jules Bloomenthal, Xerox PARC.
 * Copyright (c) Xerox Corporation, 1991.  All rights reserved.
 * Permission is granted to reproduce, use and distribute this code for
 * any and all purposes, provided that this notice appears in all copies.  */

/* A Brief Explanation
 * The code consists of a test program and the polygonizer itself.
 *
 * In the test program:
 * torus(), sphere() and blob() are primitive functions used to calculate
 *    the implicit value at a point (x,y,z)
 * triangle() is a 'callback' routine that is called by the polygonizer
 *    whenever it produces a new triangle
 * to select torus, sphere or blob, change the argument to polygonize()
 * if openGL supported, open window, establish perspective and viewport,
 *    create closed line loops, during polygonization, and slowly spin object
 * if openGL not supported, output vertices and triangles to stdout
 *
 * The main data structures in the polygonizer represent a hexahedral lattice,
 * ie, a collection of semi-adjacent cubes, represented as cube centers, corners,
 * and edges. The centers and corners are three-dimensional indices rerpesented
 * by integer i,j,k. The edges are two three-dimensional indices, represented
 * by integer i1,j1,k1,i2,j2,k2. These indices and associated data are stored
 * in hash tables.
 *
 * The client entry to the polygonizer is polygonize(), called from main().
 * This routine first allocates memory for the hash tables for the cube centers,
 * corners, and edges that define the polygonizing lattice. It then finds a start
 * point, ie, the center of the first lattice cell. It pushes this cell onto an
 * initially empty stack of cells awaiting processing. It creates the first cell
 * by computing its eight corners and assigning them an implicit value.
 *
 * polygonize() then enters a loop in which a cell is popped from the stack,
 * becoming the 'active' cell c. c is (optionally) decomposed (ie, subdivided)
 * into six tetrahedra; within each transverse tetrahedron (ie, those that
 * intersect the surface), one or two triangles are produced.
 *
 * The six faces of c are tested for intersection with the implicit surface; for
 * a transverse face, a new cube is generated and placed on the stack.
 * Some of the more important routines include:
 *
 * testface (called by polygonize): test given face for surface intersection;
 *    if transverse, create new cube by creating four new corners.
 * setcorner (called by polygonize, testface): create new cell corner at given
 *    (i,j,k), compute its implicit value, and add to corners hash table.
 * find (called by polygonize): search for point with given polarity
 * dotet (called by polygonize) set edge vertices, output triangle by
 *    invoking callback
 *
 * The section Cubical Polygonization contains routines to polygonize directly
 * from the lattice cell rather than first decompose it into tetrahedra;
 * dotet, however, is recommended over docube.
 *
 * The section Storage provides routines to handle the linked lists
 * in the hash tables.
 *
 * The section Vertices contains the following routines.
 * vertid (called by dotet): given two corner indices defining a cell edge,
 *    test whether the edge has been stored in the hash table; if so, return its
 *    associated vertex index. If not, compute intersection of edge and implicit
 *    surface, compute associated surface normal, add vertex to mesh array, and
 *    update hash tables
 * converge (called by polygonize, vertid): find surface crossing on edge */

// Structures de données

#define TET 0   /* use tetrahedral decomposition */
#define NOTET 1 /* no tetrahedral decomposition  */

#define RES 10 /* # converge iterations    */

#define L 0   /* left direction:	-x, -i  */
#define R 1   /* right direction:	+x, +i  */
#define B 2   /* bottom direction: -y, -j */
#define T 3   /* top direction:	+y, +j  */
#define N 4   /* near direction:	-z, -k  */
#define F 5   /* far direction:	+z, +k  */
#define LBN 0 /* left bottom near corner  */
#define LBF 1 /* left bottom far corner   */
#define LTN 2 /* left top near corner     */
#define LTF 3 /* left top far corner      */
#define RBN 4 /* right bottom near corner */
#define RBF 5 /* right bottom far corner  */
#define RTN 6 /* right top near corner    */
#define RTF 7 /* right top far corner     */

/* the LBN corner of cube (i, j, k), corresponds with location
 * (start.x+(i-.5)*size, start.y+(j-.5)*size, start.z+(k-.5)*size) */

#define RAND() ( ( rand() & 32767 ) / 32767. ) /* random number between 0 and 1 */
#define HASHBIT ( 5 )
#define HASHSIZE ( size_t )( 1 << ( 3 * HASHBIT ) ) /* hash table size (32768) */
#define MASK ( ( 1 << HASHBIT ) - 1 )
#define HASH( i, j, k )                                                                            \
    ( ( ( ( ( (i)&MASK ) << HASHBIT ) | ( (j)&MASK ) ) << HASHBIT ) | ( (k)&MASK ) )
#define BIT( i, bit ) ( ( ( i ) >> ( bit ) ) & 1 )
#define FLIP( i, bit ) ( ( i ) ^ 1 << ( bit ) ) /* flip the given bit of i */

#define LB 0  /* left bottom edge	*/
#define LT 1  /* left top edge	*/
#define LN 2  /* left near edge	*/
#define LF 3  /* left far edge	*/
#define RB 4  /* right bottom edge */
#define RT 5  /* right top edge	*/
#define RN 6  /* right near edge	*/
#define RF 7  /* right far edge	*/
#define BN 8  /* bottom near edge	*/
#define BF 9  /* bottom far edge	*/
#define TN 10 /* top near edge	*/
#define TF 11 /* top far edge	*/

class POINT
{ /* a three-dimensional point */
public:
    double _x, _y, _z; /* its coordinates */
    POINT()
        : _x( 0 )
        , _y( 0 )
        , _z( 0 )
    {
    }
    POINT( double x, double y, double z )
        : _x( x )
        , _y( y )
        , _z( z )
    {
    }
    friend std::ostream &operator<<( std::ostream &c, const POINT &p )
    {
        return c << "(" << p._x << ", " << p._y << ", " << p._z << ")";
    }
};

class TEST
{ /* test the function for a signed value */
public:
    POINT _p;      /* location of test */
    double _value; /* function value at p */
    int _ok;       /* if value is of correct sign */
public:
    TEST()
    {
        _value = 0;
        _ok    = 0;
    }
    TEST( POINT p, double v, int ok )
        : _p( p )
        , _value( v )
        , _ok( ok )
    {
    }
};

class VERTEX
{ /* surface vertex */
public:
    POINT _position, _normal; /* position and surface normal */
public:
    VERTEX() {}
    VERTEX( POINT pos, POINT norm )
        : _position( pos )
        , _normal( norm )
    {
    }
};

using VERTICES = std::vector<VERTEX>;

class TRIANGLE
{
public:
    int _i1, _i2, _i3;

public:
    TRIANGLE( int i1, int i2, int i3 )
        : _i1( i1 )
        , _i2( i2 )
        , _i3( i3 )
    {
    }
};

using TRIANGLES = std::vector<TRIANGLE>;

class CORNER
{ /* corner of a cube */
public:
    int _i, _j, _k;            /* (i, j, k) is index within lattice */
    double _x, _y, _z, _value; /* location and function value */
public:
    CORNER( int i, int j, int k, double x, double y, double z, double value )
        : _i( i )
        , _j( j )
        , _k( k )
        , _x( x )
        , _y( y )
        , _z( z )
        , _value( value )
    {
    }
};

class CUBE
{ /* partitioning cell (cube) */
public:
    int _i, _j, _k;        /* lattice location of cube */
    CORNER *_corners[ 8 ]; /* eight corners */
public:
    CUBE( int i, int j, int k )
        : _i( i )
        , _j( j )
        , _k( k )
    {
        for ( int i = 0; i < 8; i++ )
            _corners[ i ] = NULL;
    }
    CUBE( int i, int j, int k, CORNER *corners[] )
        : _i( i )
        , _j( j )
        , _k( k )
    {
        for ( int i = 0; i < 8; i++ )
            _corners[ i ] = corners[ i ];
    }
};

using CUBES = std::list<CUBE>;

class CENTER
{ /* list of cube locations */
public:
    int _i, _j, _k; /* cube location */
public:
    CENTER( int i, int j, int k )
        : _i( i )
        , _j( j )
        , _k( k )
    {
    }
    bool operator==( const CENTER &c )
    {
        return ( ( _i == c._i ) && ( _j == c._j ) && ( _k == c._k ) );
    }
};

using CENTERLIST = std::list<CENTER>;

class CORNERL
{ /* list of corners */
public:
    int _i, _j, _k; /* corner id */
    double _value;  /* corner value */
public:
    CORNERL( int i, int j, int k, double value )
        : _i( i )
        , _j( j )
        , _k( k )
        , _value( value )
    {
    }
    bool operator==( const CORNERL &c )
    {
        return ( ( _i == c._i ) && ( _j == c._j ) && ( _k == c._k ) );
    }
};

using CORNERLIST = std::list<CORNERL>;

class EDGE
{ /* list of edges */
public:
    int _i1, _j1, _k1, _i2, _j2, _k2; /* edge corner ids */
    int _vid;                         /* vertex id */
public:
    EDGE( int i1, int j1, int k1, int i2, int j2, int k2, int vid )
        : _i1( i1 )
        , _j1( j1 )
        , _k1( k1 )
        , _i2( i2 )
        , _j2( j2 )
        , _k2( k2 )
        , _vid( vid )
    {
    }
    bool operator==( const EDGE &e )
    {
        return ( ( _i1 == e._i1 ) && ( _j1 == e._j1 ) && ( _k1 == e._k1 ) && ( _i2 == e._i2 )
                 && ( _j2 == e._j2 ) && ( _k2 == e._k2 ) );
    }
};

using EDGELIST = std::list<EDGE>; // EDGELIST;

using INTLIST  = std::list<int>;     // INTLIST;
using INTLISTS = std::list<INTLIST>; // INTLISTS;


class PROCESS
{ /* parameters, function, storage */
public:
    PROCESS(){};
    void init_PROCESS( double ( *function )( double, double, double ),
                       int ( *triproc )( int, int, int ), double size, int bounds, int hashsize );
    double ( *function )( double, double, double );   /* implicit surface function */
    int ( *triproc )( int, int, int );                /* triangle output function */
    double size, delta;                               /* cube size, normal delta */
    int bounds;                                       /* cube range within lattice */
    POINT start;                                      /* start point on surface */
    std::unique_ptr<CUBES> cubes;                     /* active cubes */
    VERTICES vertices;                                /* surface vertices */
    std::vector<std::unique_ptr<CENTERLIST>> centers; /* cube center hash table */
    std::vector<std::unique_ptr<CORNERLIST>> corners; /* corner value hash table */
    std::vector<std::unique_ptr<EDGELIST>> edges;     /* edge and vertex id hash table */
};

// Bloomenthal

using MyMesh = Mesh::MeshT;

class bloomenthal
{
    static TRIANGLES _triangles;
    static int _glefthanded;

    static std::array<std::unique_ptr<INTLISTS>, 256> cubetable; //[ 256 ];

    /*			edge: LB, LT, LN, LF, RB, RT, RN, RF, BN, BF, TN, TF */
    static std::array<int, 12> corner1;  //[ 12 ];
    static std::array<int, 12> corner2;  //[ 12 ];
    static std::array<int, 12> leftface; //[ 12 ];
    /* face on left when going corner1 to corner2 */
    static std::array<int, 12> rightface; //[ 12 ];
    /* face on right when going corner1 to corner2 */
    std::unique_ptr<PROCESS> p;

public:
    bloomenthal();
    char const *polygonize( double ( *function )( double, double, double ), double size, int bounds,
                            double x, double y, double z, int ( *triproc )( int, int, int ),
                            int mode );
    static int triangle( int i1, int i2, int i3 );
    static int triangle2( int i1, int i2, int i3 );
    void testface( int i, int j, int k, CUBE *old, int face, int c1, int c2, int c3, int c4 );
    CORNER *setcorner( int i, int j, int k );
    TEST find( int sign, double x, double y, double z );
    int dotet( CUBE *cube, int c1, int c2, int c3, int c4 );
    int docube( CUBE *cube );
    int nextcwedge( int edge, int face );
    int otherface( int edge, int face );
    void makecubetable();
    int setcenter( int i, int j, int k );
    void setedge( std::vector<std::unique_ptr<EDGELIST>> &table, int i1, int j1, int k1, int i2,
                  int j2, int k2, int vid );
    int getedge( std::vector<std::unique_ptr<EDGELIST>> const &table, int i1, int j1, int k1,
                 int i2, int j2, int k2 );
    int vertid( CORNER *c1, CORNER *c2 );
    void vnormal( POINT *point, POINT *v );
    void converge( POINT *p1, POINT *p2, double v, double ( *function )( double, double, double ),
                   POINT *p );

    void createMeshOpenMesh( MyMesh &mesh );
};

#endif // BLOOMENTHAL_H
