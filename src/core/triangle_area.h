#pragma once

#include <cmath>
#include <type_traits>

template <typename T>
auto triangleArea( T &&firstLength, T &&secondLength, T &&thirdLength )
{
    using Type         = std::remove_reference_t<T>;
    auto halfPerimeter = static_cast<Type>( 0.5 ) * ( firstLength + secondLength + thirdLength );


    auto area = std::sqrt( halfPerimeter * ( halfPerimeter - firstLength )
                           * ( halfPerimeter - secondLength ) * ( halfPerimeter - thirdLength ) );

    return area;
}
