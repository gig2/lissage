#include "bloomenthal.h"
#include <cmath>
#include <iostream>
#include <memory>
#include <stdexcept>

using namespace std;
using namespace OpenMesh;

void PROCESS::init_PROCESS( double ( *func )( double, double, double ),
                            int ( *trip )( int, int, int ), double s, int b, int h )
{
    this->function = func;
    this->triproc  = trip;
    this->size     = s;
    this->delta    = size / (double)( RES * RES );
    this->bounds   = b;

    this->centers.reserve( h );
    this->corners.reserve( h );
    for ( int i = 0; i < h; i++ )
    {
        centers.push_back( std::make_unique<CENTERLIST>() );
        corners.push_back( std::make_unique<CORNERLIST>() );
    }

    this->edges.reserve( 2 * h ); //(EDGELIST   **) calloc(2*h,sizeof(EDGELIST *));
    for ( int i = 0; i < 2 * h; i++ )
        edges.push_back( std::make_unique<EDGELIST>() );
    this->cubes = std::make_unique<CUBES>();
}

// int bloomenthal::_gntris =0 ;
int bloomenthal::_glefthanded = 0;
std::array<int, 12> bloomenthal::corner1
    = {LBN, LTN, LBN, LBF, RBN, RTN, RBN, RBF, LBN, LBF, LTN, LTF};
std::array<int, 12> bloomenthal::corner2
    = {LBF, LTF, LTN, LTF, RBF, RTF, RTN, RTF, RBN, RBF, RTN, RTF};
std::array<int, 12> bloomenthal::leftface = {B, L, L, F, R, T, N, R, N, B, T, F};
/* face on left when going corner1 to corner2 */
std::array<int, 12> bloomenthal::rightface = {L, T, N, L, B, R, R, F, B, F, N, T};
std::array<std::unique_ptr<INTLISTS>, 256> bloomenthal::cubetable; //[ 256 ];
TRIANGLES bloomenthal::_triangles;

bloomenthal::bloomenthal() {}

int bloomenthal::triangle( int i1, int i2, int i3 )
{
    return 1;
}

int bloomenthal::triangle2( int i1, int i2, int i3 )
{
    TRIANGLE t = {i1, i2, i3};
    if ( !_glefthanded )
    {
        int temp = t._i2;
        t._i2    = t._i3;
        t._i3    = temp;
    }
    _triangles.push_back( t );
    return 1;
}

/* polygonize: polygonize the implicit surface function
 *   arguments are:
 *	 double function (x, y, z)
 *		 double x, y, z (an arbitrary 3D point)
 *	     the implicit surface function
 *	     return negative for inside, positive for outside
 *	 double size
 *	     width of the partitioning cube
 *	 int bounds
 *	     max. range of cubes (+/- on the three axes) from first cube
 *	 double x, y, z
 *	     coordinates of a starting point on or near the surface
 *	     may be defaulted to 0., 0., 0.
 *	 int triproc (i1, i2, i3, vertices)
 *		 int i1, i2, i3 (indices into the vertex array)
 *		 VERTICES vertices (the vertex array, indexed from 0)
 *	     called for each triangle
 *	     the triangle coordinates are (for i = i1, i2, i3):
 *		 vertices.ptr[i].position.x, .y, and .z
 *	     vertices are ccw when viewed from the out (positive) side
 *		 in a left-handed coordinate system
 *	     vertex normals point outwards
 *	     return 1 to continue, 0 to abort
 *	 int mode
 *	     TET: decompose cube and polygonize six tetrahedra
 *	     NOTET: polygonize cube directly
 *   returns error or NULL
 */


char const *bloomenthal::polygonize( double ( *function )( double, double, double ), double size,
                                     int bounds, double x, double y, double z,
                                     int ( *triproc )( int, int, int ), int mode )
{
    p = std::make_unique<PROCESS>();
    p->init_PROCESS( function, triproc, size, bounds, HASHSIZE );
    int noabort;
    TEST in, out;

    cout << "starting polygonization" << endl;

    bloomenthal::makecubetable();
    cout << "hash tables built" << endl;

    /* find point on surface, beginning search at (x, y, z): */
    srand( 1 );
    in  = find( 1, x, y, z );
    out = find( 0, x, y, z );
    if ( !in._ok || !out._ok )
    {
        cout << "can't find starting point" << endl;
        return "can't find starting point";
    }
    cout << "starting points : " << endl;
    cout << in._p << " - " << out._p << endl;
    converge( &in._p, &out._p, in._value, p->function, &p->start );
    cout << "converged to :" << endl;
    cout << p->start << endl;

    /* set corners of initial cube: */

    CUBE c( 0, 0, 0 );
    for ( int n = 0; n < 8; n++ )
        c._corners[ n ] = bloomenthal::setcorner( BIT( n, 2 ), BIT( n, 1 ), BIT( n, 0 ) );
    /* push initial cube on stack: */
    p->cubes->push_front( c );

    setcenter( 0, 0, 0 );

    cout << "process ..." << endl;
    while ( !p->cubes->empty() )
    { /* process active cubes till none left */
        CUBE c( p->cubes->front() );

        if ( mode == TET )
        {
            /* either decompose into tetrahedra and polygonize: */
            std::array<int, 6> result;
            result[ 0 ] = dotet( &c, LBN, LTN, RBN, LBF );
            result[ 1 ] = dotet( &c, RTN, LTN, LBF, RBN );
            result[ 2 ] = dotet( &c, RTN, LTN, LTF, LBF );
            result[ 3 ] = dotet( &c, RTN, RBN, LBF, RBF );
            result[ 4 ] = dotet( &c, RTN, LBF, LTF, RBF );
            result[ 5 ] = dotet( &c, RTN, LTF, RTF, RBF );

            if ( std::any_of( std::begin( result ), std::end( result ),
                              []( auto const &elem ) { return elem == 0; } ) )
            {
                noabort = 0;
            }
            else if ( std::any_of( std::begin( result ), std::end( result ),
                                   []( auto const &elem ) { return elem == -1; } ) )
            {
                noabort = -1;
            }
            else
            {
                noabort = 1;
            }
        }
        else
        {
            noabort = docube( &c );
        }

        if ( noabort == 0 )
            return "aborted";

        /* pop current cube from stack */
        p->cubes->pop_front();
        if ( noabort == 1 )
        {
            /* test six face directions, maybe add to stack: */
            testface( c._i - 1, c._j, c._k, &c, L, LBN, LBF, LTN, LTF );
            testface( c._i + 1, c._j, c._k, &c, R, RBN, RBF, RTN, RTF );
            testface( c._i, c._j - 1, c._k, &c, B, LBN, LBF, RBN, RBF );
            testface( c._i, c._j + 1, c._k, &c, T, LTN, LTF, RTN, RTF );
            testface( c._i, c._j, c._k - 1, &c, N, LBN, LTN, RBN, RTN );
            testface( c._i, c._j, c._k + 1, &c, F, LBF, LTF, RBF, RTF );
        }
    }
    cout << "nombre de sommets : " << p->vertices.size() << endl;
    cout << "nombre de triangles : " << _triangles.size() << endl;
    return NULL;
}


/* testface: given cube at lattice (i, j, k), and four corners of face,
 * if surface crosses face, compute other four corners of adjacent cube
 * and add new cube to cube stack */
void bloomenthal::testface( int i, int j, int k, CUBE *old, int face, int c1, int c2, int c3,
                            int c4 )
{
    static int facebit[ 6 ] = {2, 2, 1, 1, 0, 0};
    int n, pos = old->_corners[ c1 ]->_value > 0.0 ? 1 : 0, bit = facebit[ face ];

    /* test if no surface crossing, cube out of bounds, or already visited: */
    if ( ( old->_corners[ c2 ]->_value > 0 ) == pos && ( old->_corners[ c3 ]->_value > 0 ) == pos
         && ( old->_corners[ c4 ]->_value > 0 ) == pos )
        return;
    if ( abs( i ) > p->bounds || abs( j ) > p->bounds || abs( k ) > p->bounds )
        return;
    if ( setcenter( i, j, k ) )
        return;

    /* create new cube: */
    CUBE newc( i, j, k );
    for ( n = 0; n < 8; n++ )
        newc._corners[ n ] = NULL;
    newc._corners[ FLIP( c1, bit ) ] = old->_corners[ c1 ];
    newc._corners[ FLIP( c2, bit ) ] = old->_corners[ c2 ];
    newc._corners[ FLIP( c3, bit ) ] = old->_corners[ c3 ];
    newc._corners[ FLIP( c4, bit ) ] = old->_corners[ c4 ];
    for ( n = 0; n < 8; n++ )
        if ( newc._corners[ n ] == NULL )
            newc._corners[ n ] = setcorner( i + BIT( n, 2 ), j + BIT( n, 1 ), k + BIT( n, 0 ) );

    /*add cube to top of stack: */
    p->cubes->push_front( newc );
}

/* setcorner: return corner with the given lattice location
   set (and cache) its function value */

CORNER *bloomenthal::setcorner( int i, int j, int k )
{
    /* for speed, do corner value caching here */
    int index     = HASH( i, j, k );
    CORNERLIST *l = p->corners[ index ].get();
    CORNERL c( i, j, k, 0 );
    // Does the corner already exist in the corner list
    list<CORNERL>::iterator it = std::find( l->begin(), l->end(), c );
    if ( it != l->end() )
    {
        double x = p->start._x + ( (double)i - .5 ) * p->size,
               y = p->start._y + ( (double)j - .5 ) * p->size,
               z = p->start._z + ( (double)k - .5 ) * p->size;
        return new CORNER( ( *it )._i, ( *it )._j, ( *it )._k, x, y, z, ( *it )._value );
    }

    // If not create it and add it
    double x   = p->start._x + ( (double)i - .5 ) * p->size,
           y   = p->start._y + ( (double)j - .5 ) * p->size,
           z   = p->start._z + ( (double)k - .5 ) * p->size;
    CORNER *cc = new CORNER( i, j, k, x, y, z, p->function( x, y, z ) );
    l->push_front( c );
    return cc;
}

/* find: search for point with value of given sign (0: neg, 1: pos) */

TEST bloomenthal::find( int sign, double x, double y, double z )
{
    double range = p->size;
    int ok       = 1;
    for ( int i = 0; i < 10000; i++ )
    {
        POINT pt( x + range * ( RAND() - 0.5 ), y + range * ( RAND() - 0.5 ),
                  z + range * ( RAND() - 0.5 ) );
        TEST test( pt, p->function( pt._x, pt._y, pt._z ), ok );

        if ( sign == ( test._value > 0.0 ) )
            return test;
        range = range * 1.0005; /* slowly expand search outwards */
    }
    return TEST();
}

/**** Tetrahedral Polygonization ****/


/* dotet: triangulate the tetrahedron
 * b, c, d should appear clockwise when viewed from a
 * return 0 if client aborts, 1 otherwise */

int bloomenthal::dotet( CUBE *cube, int c1, int c2, int c3, int c4 )
{
    CORNER *a = cube->_corners[ c1 ];
    CORNER *b = cube->_corners[ c2 ];
    CORNER *c = cube->_corners[ c3 ];
    CORNER *d = cube->_corners[ c4 ];
    int index = 0, apos, bpos, cpos, dpos, e1, e2, e3, e4, e5, e6;
    if ( apos = ( a->_value > 0.0 ); apos )
        index += 8;
    if ( bpos = ( b->_value > 0.0 ); bpos )
        index += 4;
    if ( cpos = ( c->_value > 0.0 ); cpos )
        index += 2;
    if ( dpos = ( d->_value > 0.0 ); dpos )
        index += 1;
    /* index is now 4-bit number representing one of the 16 possible cases */
    if ( apos != bpos )
        e1 = vertid( a, b );
    if ( apos != cpos )
        e2 = vertid( a, c );
    if ( apos != dpos )
        e3 = vertid( a, d );
    if ( bpos != cpos )
        e4 = vertid( b, c );
    if ( bpos != dpos )
        e5 = vertid( b, d );
    if ( cpos != dpos )
        e6 = vertid( c, d );
    /* 14 productive tetrahedral cases (0000 and 1111 do not yield polygons */
    switch ( index )
    {
        case 1: return p->triproc( e5, e6, e3 );
        case 2: return p->triproc( e2, e6, e4 );
        case 3: return p->triproc( e3, e5, e4 ) && p->triproc( e3, e4, e2 );
        case 4: return p->triproc( e1, e4, e5 );
        case 5: return p->triproc( e3, e1, e4 ) && p->triproc( e3, e4, e6 );
        case 6: return p->triproc( e1, e2, e6 ) && p->triproc( e1, e6, e5 );
        case 7: return p->triproc( e1, e2, e3 );
        case 8: return p->triproc( e1, e3, e2 );
        case 9: return p->triproc( e1, e5, e6 ) && p->triproc( e1, e6, e2 );
        case 10: return p->triproc( e1, e3, e6 ) && p->triproc( e1, e6, e4 );
        case 11: return p->triproc( e1, e5, e4 );
        case 12: return p->triproc( e3, e2, e4 ) && p->triproc( e3, e4, e5 );
        case 13: return p->triproc( e6, e2, e4 );
        case 14: return p->triproc( e5, e3, e6 );
    }
    return -1;
}

/**** Cubical Polygonization (optional) ****/



/* docube: triangulate the cube directly, without decomposition */

int bloomenthal::docube( CUBE *cube )
{
    INTLISTS::iterator polys;
    int i, index = 0;
    for ( i = 0; i < 8; i++ )
        if ( cube->_corners[ i ]->_value > 0.0 )
            index += ( 1 << i );

    auto &cubeList = *cubetable[ index ];

    for ( auto polys = cubeList.begin(); polys != cubeList.end(); polys++ )
    {
        int a = -1, b = -1, count = 0;
        for ( list<int>::iterator edges = polys->begin(); edges != polys->end(); edges++ )
        {
            CORNER *c1 = cube->_corners[ corner1[ *edges ] ];
            CORNER *c2 = cube->_corners[ corner2[ *edges ] ];
            int c      = vertid( c1, c2 );
            if ( ++count > 2 && !p->triproc( a, b, c ) )
                return 0;
            if ( count < 3 )
                a = b;
            b = c;
        }
    }
    return 1;
}


/* nextcwedge: return next clockwise edge from given edge around given face */

int bloomenthal::nextcwedge( int edge, int face )
{
    switch ( edge )
    {
        case LB: return ( face == L ) ? LF : BN;
        case LT: return ( face == L ) ? LN : TF;
        case LN: return ( face == L ) ? LB : TN;
        case LF: return ( face == L ) ? LT : BF;
        case RB: return ( face == R ) ? RN : BF;
        case RT: return ( face == R ) ? RF : TN;
        case RN: return ( face == R ) ? RT : BN;
        case RF: return ( face == R ) ? RB : TF;
        case BN: return ( face == B ) ? RB : LN;
        case BF: return ( face == B ) ? LB : RF;
        case TN: return ( face == T ) ? LT : RN;
        case TF: return ( face == T ) ? RT : LF;
        default: throw std::runtime_error( "Edge case not found" );
    }
}


/* otherface: return face adjoining edge that is not the given face */

int bloomenthal::otherface( int edge, int face )
{
    int other = leftface[ edge ];
    return face == other ? rightface[ edge ] : other;
}


/* makecubetable: create the 256 entry table for cubical polygonization */

void bloomenthal::makecubetable()
{
    int i, e, c, done[ 12 ], pos[ 8 ];
    for ( i = 0; i < 256; i++ )
    {
        for ( e = 0; e < 12; e++ )
            done[ e ] = 0;
        for ( c = 0; c < 8; c++ )
            pos[ c ] = BIT( i, c );
        for ( e = 0; e < 12; e++ )
            if ( !done[ e ] && ( pos[ corner1[ e ] ] != pos[ corner2[ e ] ] ) )
            {
                auto ints  = std::make_unique<INTLIST>();
                auto lists = std::make_unique<INTLISTS>(); // new INTLISTS;
                int start = e, edge = e;
                /* get face that is to right of edge from pos to neg corner: */
                int face = pos[ corner1[ e ] ] ? rightface[ e ] : leftface[ e ];
                while ( 1 )
                {
                    edge         = nextcwedge( edge, face );
                    done[ edge ] = 1;
                    if ( pos[ corner1[ edge ] ] != pos[ corner2[ edge ] ] )
                    {
                        // INTLIST *tmp = ints;
                        // ints = new INTLIST;
                        ints->push_front( edge );
                        if ( edge == start )
                            break;
                        face = otherface( edge, face );
                    }
                }
                lists->push_front( *ints );
                cubetable[ i ] = std::move( lists ); /* add ints to head of table entry */
            }
    }
}

/**** Storage ****/

/* setcenter: set (i,j,k) entry of table[]
 * return 1 if already set; otherwise, set and return 0 */

int bloomenthal::setcenter( int i, int j, int k )
{
    int index     = HASH( i, j, k );
    CENTERLIST *q = p->centers[ index ].get();

    list<CENTER>::iterator it = std::find( q->begin(), q->end(), CENTER( i, j, k ) );
    if ( it != q->end() )
    {
        return 1;
    }

    q->push_front( CENTER( i, j, k ) );
    return 0;
}


/* setedge: set vertex id for edge */

void bloomenthal::setedge( std::vector<std::unique_ptr<EDGELIST>> &table, int i1, int j1, int k1,
                           int i2, int j2, int k2, int vid )
{
    unsigned int index;
    if ( i1 > i2 || ( i1 == i2 && ( j1 > j2 || ( j1 == j2 && k1 > k2 ) ) ) )
    {
        int t = i1;
        i1    = i2;
        i2    = t;
        t     = j1;
        j1    = j2;
        j2    = t;
        t     = k1;
        k1    = k2;
        k2    = t;
    }
    index = HASH( i1, j1, k1 ) + HASH( i2, j2, k2 );
    table[ index ]->push_front( EDGE( i1, j1, k1, i2, j2, k2, vid ) );
}


/* getedge: return vertex id for edge; return -1 if not set */

int bloomenthal::getedge( std::vector<std::unique_ptr<EDGELIST>> const &table, int i1, int j1,
                          int k1, int i2, int j2, int k2 )
{
    EDGELIST *q;
    if ( i1 > i2 || ( i1 == i2 && ( j1 > j2 || ( j1 == j2 && k1 > k2 ) ) ) )
    {
        int t = i1;
        i1    = i2;
        i2    = t;
        t     = j1;
        j1    = j2;
        j2    = t;
        t     = k1;
        k1    = k2;
        k2    = t;
    };
    q                       = table[ HASH( i1, j1, k1 ) + HASH( i2, j2, k2 ) ].get();
    list<EDGE>::iterator it = std::find( q->begin(), q->end(), EDGE( i1, j1, k1, i2, j2, k2, 0 ) );
    if ( it != q->end() )
        return ( *it )._vid;
    return -1;
}


/**** Vertices ****/


/* vertid: return index for vertex on edge:
 * c1->value and c2->value are presumed of different sign
 * return saved index if any; else compute vertex and save */

int bloomenthal::vertid( CORNER *c1, CORNER *c2 )
{
    VERTEX v;

    int vid = getedge( p->edges, c1->_i, c1->_j, c1->_k, c2->_i, c2->_j, c2->_k );
    if ( vid != -1 )
    {
        return vid; /* previously computed */
    }
    POINT a( c1->_x, c1->_y, c1->_z ), b( c2->_x, c2->_y, c2->_z );
    converge( &a, &b, c1->_value, p->function, &v._position ); /* position */
    vnormal( &v._position, &v._normal );                       /* normal */
    p->vertices.push_back( v );                                /* save vertex */
    vid = p->vertices.size() - 1;
    setedge( p->edges, c1->_i, c1->_j, c1->_k, c2->_i, c2->_j, c2->_k, vid );
    return vid;
}


/* vnormal: compute unit length surface normal at point */

void bloomenthal::vnormal( POINT *point, POINT *v )
{
    double f = p->function( point->_x, point->_y, point->_z );
    v->_x    = p->function( point->_x + p->delta, point->_y, point->_z ) - f;
    v->_y    = p->function( point->_x, point->_y + p->delta, point->_z ) - f;
    v->_z    = p->function( point->_x, point->_y, point->_z + p->delta ) - f;
    f        = sqrt( v->_x * v->_x + v->_y * v->_y + v->_z * v->_z );
    if ( f != 0.0 )
    {
        v->_x /= f;
        v->_y /= f;
        v->_z /= f;
    }
}


/* converge: from two points of differing sign, converge to zero crossing */

void bloomenthal::converge( POINT *p1, POINT *p2, double v,
                            double ( *function )( double, double, double ), POINT *p )
{
    int i = 0;
    POINT pos, neg;
    if ( v < 0 )
    {
        pos = POINT( p2->_x, p2->_y, p2->_z );
        neg = POINT( p1->_x, p1->_y, p1->_z );
    }
    else
    {
        neg = POINT( p2->_x, p2->_y, p2->_z );
        pos = POINT( p1->_x, p1->_y, p1->_z );
    }
    while ( 1 )
    {
        p->_x = 0.5 * ( pos._x + neg._x );
        p->_y = 0.5 * ( pos._y + neg._y );
        p->_z = 0.5 * ( pos._z + neg._z );
        if ( i++ == RES )
            return;
        if ( ( function( p->_x, p->_y, p->_z ) ) > 0.0 )
        {
            pos._x = p->_x;
            pos._y = p->_y;
            pos._z = p->_z;
        }
        else
        {
            neg._x = p->_x;
            neg._y = p->_y;
            neg._z = p->_z;
        }
    }
}

// OpenMesh output

void bloomenthal::createMeshOpenMesh( MyMesh &mesh )
{
    cout << "Starting createMesh ..." << endl;
    // Tableau de vertex handles pour chaque sommet
    VertexHandle OMVert[ p->vertices.size() ];

    // Ajout de sommets (avec normale) au maillage
    for ( int i = 0; i < p->vertices.size(); i++ )
    {
        VERTEX v( p->vertices[ i ] );
        MyMesh::Point pt( v._position._x, v._position._y, v._position._z ),
            n( v._normal._x, v._normal._y, v._normal._z );
        OMVert[ i ] = mesh.add_vertex( pt );
        mesh.set_normal( OMVert[ i ], n );
    }
    cout << p->vertices.size() << " vertices added" << endl;
    // Ajout des triangles
    vector<MyMesh::VertexHandle> f;
    for ( int i = 0; i < this->_triangles.size(); i++ )
    {
        //        cout << "adding face " << i << " : " << _triangles._ptr[i]._i1 << ", " <<
        //        _triangles._ptr[i]._i2 << ", " << _triangles._ptr[i]._i3 << endl ;
        f.clear();

        f.push_back( OMVert[ _triangles[ i ]._i1 ] );
        f.push_back( OMVert[ _triangles[ i ]._i2 ] );
        f.push_back( OMVert[ _triangles[ i ]._i3 ] );
        mesh.add_face( f );
    }
    cout << "end createMesh ..." << endl;
}
