#pragma once

#include <algorithm>
#include <vector>

#include <Eigen/Dense>

template <typename MeshType>
void applyValence( MeshType &mesh )
{
    //
    using Color = typename MeshType::Color;

    std::vector<int> valence;

    std::transform( mesh.vertices_begin(), mesh.vertices_end(), std::back_inserter( valence ),
                    [&mesh]( auto &vertex ) { return mesh.valence( vertex ); } );

    auto minmax = std::minmax_element( std::begin( valence ), std::end( valence ) );


    std::for_each( mesh.vertices_begin(), mesh.vertices_end(), [&mesh, &minmax]( auto &vertex ) {
        Color color0{0.0, .0, 1.0};
        Color color1{1.0, .0, 0.0};

        Color color{0., 0., 0.};

        auto valence = mesh.valence( vertex );


        float coeff
            = ( valence - *minmax.first ) / static_cast<float>( *minmax.second - *minmax.first );

        for ( int iCol = 0; iCol < 3; ++iCol )
        {
            color[ iCol ] = ( 1. - coeff ) * color0[ iCol ] + coeff * color1[ iCol ];
        }

        mesh.set_color( vertex, color );
    } );


    // end
}
