
#include "fill_hole.h"
#include "tag_hole.h"

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <algorithm>
#include <array>
#include <cmath>
#include <functional>
#include <vector>

#include <OpenMesh/Core/Geometry/Vector11T.hh>
#include <OpenMesh/Core/Mesh/Traits.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>



using testing::Eq;
using testing::FloatEq;

using MeshT = OpenMesh::TriMesh_ArrayKernelT<OpenMesh::DefaultTraits>;


void addFace0( MeshT& mesh, std::array<MeshT::VertexHandle, 8> const& vhandle )
{
    std::vector<MeshT::VertexHandle> faceVhandles;

    faceVhandles.push_back( vhandle[ 0 ] );
    faceVhandles.push_back( vhandle[ 1 ] );
    faceVhandles.push_back( vhandle[ 2 ] );
    faceVhandles.push_back( vhandle[ 3 ] );
    mesh.add_face( faceVhandles );
}
void addFace1( MeshT& mesh, std::array<MeshT::VertexHandle, 8> const& vhandle )
{
    std::vector<MeshT::VertexHandle> faceVhandles;

    faceVhandles.push_back( vhandle[ 7 ] );
    faceVhandles.push_back( vhandle[ 6 ] );
    faceVhandles.push_back( vhandle[ 5 ] );
    faceVhandles.push_back( vhandle[ 4 ] );
    mesh.add_face( faceVhandles );
}

void addFace2( MeshT& mesh, std::array<MeshT::VertexHandle, 8> const& vhandle )

{
    std::vector<MeshT::VertexHandle> faceVhandles;

    faceVhandles.push_back( vhandle[ 1 ] );
    faceVhandles.push_back( vhandle[ 0 ] );
    faceVhandles.push_back( vhandle[ 4 ] );
    faceVhandles.push_back( vhandle[ 5 ] );
    mesh.add_face( faceVhandles );
}
void addFace3( MeshT& mesh, std::array<MeshT::VertexHandle, 8> const& vhandle )
{
    std::vector<MeshT::VertexHandle> faceVhandles;

    faceVhandles.push_back( vhandle[ 2 ] );
    faceVhandles.push_back( vhandle[ 1 ] );
    faceVhandles.push_back( vhandle[ 5 ] );
    faceVhandles.push_back( vhandle[ 6 ] );
    mesh.add_face( faceVhandles );
}
void addFace4( MeshT& mesh, std::array<MeshT::VertexHandle, 8> const& vhandle )

{
    std::vector<MeshT::VertexHandle> faceVhandles;

    faceVhandles.push_back( vhandle[ 3 ] );
    faceVhandles.push_back( vhandle[ 2 ] );
    faceVhandles.push_back( vhandle[ 6 ] );
    faceVhandles.push_back( vhandle[ 7 ] );
    mesh.add_face( faceVhandles );
}

void addFace5( MeshT& mesh, std::array<MeshT::VertexHandle, 8> const& vhandle )
{
    std::vector<MeshT::VertexHandle> faceVhandles;

    faceVhandles.push_back( vhandle[ 0 ] );
    faceVhandles.push_back( vhandle[ 3 ] );
    faceVhandles.push_back( vhandle[ 7 ] );
    faceVhandles.push_back( vhandle[ 4 ] );
    mesh.add_face( faceVhandles );
}

std::array<MeshT::VertexHandle, 8> cubeVerticesHandle( MeshT& mesh )
{
    std::array<MeshT::VertexHandle, 8> vhandle;

    vhandle[ 0 ] = mesh.add_vertex( MeshT::Point{-1, -1, 1} );
    vhandle[ 1 ] = mesh.add_vertex( MeshT::Point{1, -1, 1} );
    vhandle[ 2 ] = mesh.add_vertex( MeshT::Point{1, 1, 1} );
    vhandle[ 3 ] = mesh.add_vertex( MeshT::Point{-1, 1, 1} );
    vhandle[ 4 ] = mesh.add_vertex( MeshT::Point{-1, -1, -1} );
    vhandle[ 5 ] = mesh.add_vertex( MeshT::Point{1, -1, -1} );
    vhandle[ 6 ] = mesh.add_vertex( MeshT::Point{1, 1, -1} );
    vhandle[ 7 ] = mesh.add_vertex( MeshT::Point{-1, 1, -1} );

    return vhandle;
}

MeshT cubeWithZeroHole()
{
    MeshT mesh;

    // let's generate a cube
    // without one face

    auto vhandle = cubeVerticesHandle( mesh );

    addFace0( mesh, vhandle );
    addFace1( mesh, vhandle );
    addFace2( mesh, vhandle );
    addFace3( mesh, vhandle );
    addFace4( mesh, vhandle );
    addFace5( mesh, vhandle );

    return mesh;
}

MeshT cubeWithOneHole()
{
    MeshT mesh;

    // let's generate a cube
    // without one face

    auto vhandle = cubeVerticesHandle( mesh );

    addFace0( mesh, vhandle );
    addFace1( mesh, vhandle );
    addFace2( mesh, vhandle );
    addFace3( mesh, vhandle );
    addFace4( mesh, vhandle );

    return mesh;
}

MeshT cubeWithTwoHoles()
{
    MeshT mesh;

    // let's generate a cube
    // without one face

    auto vhandle = cubeVerticesHandle( mesh );


    // face 3 and face 5 do not share one common vertex
    // so by eliminating them we made 2 holes
    addFace0( mesh, vhandle );
    addFace1( mesh, vhandle );
    addFace2( mesh, vhandle );
    addFace4( mesh, vhandle );

    return mesh;
}


TEST( tagHole, withZeroHoleCountZeroHole )
{
    //
    auto mesh = cubeWithZeroHole();

    TagHole<MeshT> tag{mesh};

    int numberOfHole = tag.numberOfHoles();

    EXPECT_THAT( numberOfHole, Eq( 0 ) );
}
TEST( tagHole, withOneHoleCountOneHole )
{
    //

    auto mesh = cubeWithOneHole();

    TagHole<MeshT> tag{mesh};

    int numberOfHole = tag.numberOfHoles();


    EXPECT_THAT( numberOfHole, Eq( 1 ) );
}
TEST( tagHole, withTwoHolesCountTwoHoles )
{
    //

    auto mesh = cubeWithTwoHoles();

    TagHole<MeshT> tag{mesh};

    int numberOfHole = tag.numberOfHoles();


    EXPECT_THAT( numberOfHole, Eq( 2 ) );
}
