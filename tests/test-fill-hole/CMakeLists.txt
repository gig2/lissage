add_executable(test-fill-hole
  main.cpp
  test_fill-hole.cpp
  )

target_include_directories(test-fill-hole PRIVATE
  $<BUILD_INTERFACE:${gtest_SOURCE_DIR}/include>
  $<BUILD_INTERFACE:${gmock_SOURCE_DIR}/include>)

target_link_libraries(test-fill-hole PRIVATE
  lissage_core
  gtest
  gmock)

add_test(NAME test-fill-hole COMMAND test-fill-hole
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
