
#include "laplace.h"

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <algorithm>
#include <array>
#include <cmath>
#include <functional>
#include <vector>

#include <OpenMesh/Core/Geometry/Vector11T.hh>
#include <OpenMesh/Core/Mesh/Traits.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>



using testing::Eq;
using testing::FloatEq;

using MeshT = OpenMesh::TriMesh_ArrayKernelT<OpenMesh::DefaultTraits>;

TEST( theAreaOfOneTriangleRect, EqualOneHalf )
{
    //

    MeshT mesh;

    std::array<MeshT::VertexHandle, 3> vhandle;

    vhandle[ 0 ] = mesh.add_vertex( MeshT::Point{0., 0., 0.} );
    vhandle[ 1 ] = mesh.add_vertex( MeshT::Point{0., 1., 0.} );
    vhandle[ 2 ] = mesh.add_vertex( MeshT::Point{1., 0., 0.} );

    std::vector<MeshT::VertexHandle> faceVhandles;

    faceVhandles.push_back( vhandle[ 1 ] );
    faceVhandles.push_back( vhandle[ 0 ] );
    faceVhandles.push_back( vhandle[ 2 ] );

    mesh.add_face( faceVhandles );


    auto vIt = mesh.vertices_begin();

    LaplaceBeltramy<MeshT> laplace{mesh};

    auto area = laplace.computeArea( *vIt );


    EXPECT_THAT( area, FloatEq( 0.5f ) );
}
TEST( theAreaOfTwoTriangleRect, EqualOne )
{
    //

    MeshT mesh;

    std::array<MeshT::VertexHandle, 3> vhandle;

    vhandle[ 0 ] = mesh.add_vertex( MeshT::Point{0., 0., 0.} );
    vhandle[ 1 ] = mesh.add_vertex( MeshT::Point{0., 1., 0.} );
    vhandle[ 2 ] = mesh.add_vertex( MeshT::Point{1., 0., 0.} );
    vhandle[ 3 ] = mesh.add_vertex( MeshT::Point{1., 1., 0.} );

    std::vector<MeshT::VertexHandle> faceVhandles;

    faceVhandles.push_back( vhandle[ 1 ] );
    faceVhandles.push_back( vhandle[ 0 ] );
    faceVhandles.push_back( vhandle[ 2 ] );

    mesh.add_face( faceVhandles );

    faceVhandles.clear();
    faceVhandles.push_back( vhandle[ 2 ] );
    faceVhandles.push_back( vhandle[ 3 ] );
    faceVhandles.push_back( vhandle[ 1 ] );

    mesh.add_face( faceVhandles );


    auto vIt = std::next( mesh.vertices_begin() );

    LaplaceBeltramy<MeshT> laplace{mesh};

    auto area = laplace.computeArea( *vIt );


    EXPECT_THAT( area, FloatEq( 1.f ) );
}
