
#include "laplace.h"

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <algorithm>
#include <array>
#include <cmath>
#include <functional>
#include <vector>

#include <OpenMesh/Core/Geometry/Vector11T.hh>
#include <OpenMesh/Core/Mesh/Traits.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>



using testing::Eq;
using testing::FloatEq;

using MeshT = OpenMesh::TriMesh_ArrayKernelT<OpenMesh::DefaultTraits>;

TEST( theCotanOfPiOver4, isEqualToTheActualCotanOfPiOver4 )
{
    MeshT mesh;

    std::array<MeshT::VertexHandle, 3> vhandle;

    vhandle[ 0 ] = mesh.add_vertex( MeshT::Point{0., 0., 0.} );
    vhandle[ 1 ] = mesh.add_vertex( MeshT::Point{0., 1., 0.} );
    vhandle[ 2 ] = mesh.add_vertex( MeshT::Point{1., 0., 0.} );

    std::vector<MeshT::VertexHandle> faceVhandles;

    faceVhandles.push_back( vhandle[ 1 ] );
    faceVhandles.push_back( vhandle[ 0 ] );
    faceVhandles.push_back( vhandle[ 2 ] );

    mesh.add_face( faceVhandles );


    auto v0 = mesh.vertices_begin();
    auto v1 = std::next( v0 );
    auto v2 = std::next( v1 );

    auto e1 = mesh.point( *v0 ) - mesh.point( *v1 );
    auto e2 = mesh.point( *v2 ) - mesh.point( *v1 );

    // we want cotan(anglePi/4)

    auto cotan = cotangente( e1, e2 );

    auto expected = 1.f / std::tan( M_PI / 4.f );

    EXPECT_THAT( cotan, FloatEq( expected ) );
}
