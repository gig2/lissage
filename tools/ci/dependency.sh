#!/bin/sh
apt-get update -qq
apt-get install -qq -y cmake
apt-get install -q -y git
apt-get install -qq -y ninja-build
apt-get install -qq -y build-essential
apt-get install -qq -y libglew-dev
apt-get install -qq -y libglm-dev
apt-get install -qq -y clang
apt-get install -qq -y meson
apt-get install -qq -y qtbase5-dev
apt-get install -qq -y ninja-build
apt-get install -qq -y libglm-dev
apt-get install -qq -y libqt5opengl5-dev
git clone https://github.com/g-truc/glm glm --branch 0.9.9.3 && cd glm && cmake . -GNinja -DGLM_TEST_ENABLE=OFF && ninja && ninja install && cd ..
if [ -d build-eigen ]; then
    rm -rf build-eigen
fi
git clone https://github.com/eigenteam/eigen-git-mirror eigen && mkdir build-eigen && cd build-eigen && cmake ../eigen -GNinja && ninja && ninja install && cd ..

