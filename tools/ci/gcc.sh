#!/bin/sh

mkdir build && cd build && cmake .. -DBUILD_APPS=OFF -Dtest=ON -GNinja && ninja
